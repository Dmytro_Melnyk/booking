import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<String> station = new ArrayList<>();
        station.add("Lviv");
        station.add("Syhiv");
        station.add("Dnipro");
        station.add("Kyiv");
        station.add("Venice");
        station.add("Kharkiv");
        station.add("Odessa");

        List<Rout> rout = new ArrayList<>();

        rout.add(new Rout(station.subList(0, 3), Arrays.asList(100, 20, 10, 0)));
        rout.add(new Rout(station.subList(3, 5), Arrays.asList(100, 20, 10, 0)));
        rout.add(new Rout(station.subList(5, 6), Arrays.asList(100, 20, 10, 0)));

        for(Rout r: rout){
            System.out.println(r.hasStation(station.get(3)) && !r.isEnd(station.get(3)));
        }
    }
}
