import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Rout {
    private String name;
    private Map<String, Integer> stop;

    public Rout(List<String> station, List<Integer> distance){
        this.name = station.get(0) + "/" + station.get(station.size() - 1);
        this.stop = new HashMap<>();

        for(int i = 0; i < station.size(); i++){
            this.stop.put(station.get(i), distance.get(i));
        }
    }

    //check except final station
    public boolean hasStation(String station){
        return this.stop.containsKey(station);
    }

    //depricated
//    public boolean isEnd(String station){
//        return stop.get(station) == stop.size() - 1;
//    }

    @Override
    public String toString(){
        return name;
    }
}
